package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.*;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PhotoMapper {

    @Autowired
    private CommentMapper commentMapper;

    public PhotoFullDto map(PhotoEntity entity) {
        PhotoFullDto dto = new PhotoFullDto();
        dto.setId(entity.getId());
        dto.setLink(entity.getLink());
        dto.setName(entity.getName());
        dto.setRating(entity.getRating());

        List<CommentFullDto> commentDtos = commentMapper.map(entity.getComments());
        dto.setComment(commentDtos);
        return dto;
    }

    public List<PhotoShortDto> map(List<PhotoEntity> entities) {
        List<PhotoShortDto> dtos = new ArrayList<>();
        for (PhotoEntity entity : entities) {
            PhotoShortDto dto = new PhotoShortDto();
            dto.setId(entity.getId());
            dto.setLink(entity.getLink());
            dto.setName(entity.getName());
            dto.setRating(entity.getRating());

            dtos.add(dto);
        }
        return dtos;
    }

    public PhotoEntity map(PhotoCreateDto dto) {
        PhotoEntity entity = new PhotoEntity();
        entity.setLink(dto.getLink());
        entity.setName(dto.getName());
        entity.setRating(dto.getRating());
        return entity;
    }
}
