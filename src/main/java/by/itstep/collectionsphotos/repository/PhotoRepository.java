package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;

import java.util.List;

public interface PhotoRepository {

    public PhotoEntity findById(int id);

    public List<PhotoEntity> findAll();

    public PhotoEntity create(PhotoEntity entity);

    public PhotoEntity update(PhotoEntity entity);

    public void deleteById(int id);

    public void deleteAll();
}
