package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CommentEntity;
import java.util.List;

public interface CommentRepository {

    public CommentEntity findById(int id);

    public List<CommentEntity> findAll();

    public CommentEntity create(CommentEntity entity);

    public CommentEntity update(CommentEntity entity);

    public void deleteById(int id);

    public void deleteAll();
}
