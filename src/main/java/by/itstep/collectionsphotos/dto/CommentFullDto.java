package by.itstep.collectionsphotos.dto;

import lombok.Data;

@Data
public class CommentFullDto {

    private Integer id;
    private String message;
    private Integer photoId;
    private UserShortDto user;
}
