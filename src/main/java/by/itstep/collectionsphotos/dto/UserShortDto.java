package by.itstep.collectionsphotos.dto;

import lombok.Data;

@Data
public class UserShortDto {
    // UserDto будет отправлен клиенту для отображения СПИСКА всех юзеров

    private Integer id;
    private String name;
    private String login;
    private String email;

}