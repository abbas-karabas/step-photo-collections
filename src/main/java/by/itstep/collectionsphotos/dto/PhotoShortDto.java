package by.itstep.collectionsphotos.dto;

import lombok.Data;

@Data
public class PhotoShortDto {

    private Integer id;
    private String link;
    private String name;
    private Integer rating;

}
